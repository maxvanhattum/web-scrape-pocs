from scrapy_time.spiders.quotes_spider import QuotesSpider
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

spider = QuotesSpider
process = CrawlerProcess(get_project_settings())
process.crawl(spider, urls=[
            'https://www.bol.com/nl/nl/l/te-reserveren-muziek/3132+7288/',
        ])

process.start()
