import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class SeleniumImpl:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Chrome(
            os.path.join(os.path.dirname(__file__), './resources/chromedriver.exe'),
            options=options)

    def get_webpage(self, url: str):
        self.driver.get(url)
        page = self.driver.page_source
        self.driver.quit()
        return page
