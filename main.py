from requests_impl import RequestsImpl
from selenium_impl import SeleniumImpl
import timeit


def selenium_time(url: str):
    run = SeleniumImpl()
    return run.get_webpage(url)


def scrapy_splash_time(url: str):
    return "html document"


def requests_time(url: str):
    run = RequestsImpl()
    return run.get_webpage(url)


if __name__ == '__main__':
    url = "https://bol.com"
    print(timeit.timeit("requests_time(url)", globals=locals(), number=5)/5)

    print(timeit.timeit("selenium_time(url)", globals=locals(), number=1))


