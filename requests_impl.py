import requests


class RequestsImpl:
    def get_webpage(self, url: str):
        page = requests.get(url).content
        return page
